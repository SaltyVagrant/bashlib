#!/usr/bin/env bash
# vi :set ft=bash:

faketty () {
  script -qefc "$(printf "%q " "$@")" /dev/null
}


trap_push () {
  local trap_cmd
  trap_cmd=$1; shift || fatal "${FUNCNAME} usage error"
  for trap_add_signal in "$@"; do
    trap -- "$(
      trap -p "${trap_add_signal}" | cut -d" " -f3
      printf '%s;' "${trap_cmd}"
      )" "${trap_add_signal}" || fatal "unable to add trap for ${trap_add_signal}"
  done
}

declare -f -t trap_push

trap_trace_signals() {
  for sig ; do
    trap_push "trap_log_signal $sig" "$sig"
  done
}

trap_log_signal() {
  info "Trapped: $1"
}


trap_trace_all_signals () {
  # shellcheck disable=SC2046
  trap_trace_signals $(trap -l | awk 'BEGIN{RS=")"} /[A-Z]/{print $1}')
}

progress_dot () {
  printf '.'
}

